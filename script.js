// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
// AJAX - це технологія, яка дозволяє надсилати запит та отримувати відповідь від сервера без перезавантаження
// сторінки. Таким чином існує можливість отримувати дані, які можна додавати на сторінку (при взаємодіїї 
// користувача з елементами сторінки): виводити або прибирати дані, оновлювати тощо.

// Два файла з JS-кодом. Виконують одне і теж. Script.js - використовується async/await, script2 - promise().then()
// До html підключений script.js.


async function getStraWarsFilms() {

    document.body.innerHTML = "";
    let request = await fetch("https://ajax.test-danit.com/api/swapi/films");
    let data = await request.json();  
    let characters = await f1(data);
    let actors = await getActors(characters);       
    writeActors(actors);
}


async function getActors(characters) {

    let all_actors = [];
    let i = 0;

    for (let elem of characters) { 
        let ul = document.createElement("ul");
        for (let arr_elem of elem) {

            let actor_request = await fetch(arr_elem);
            let actor = await actor_request.json();                  

            await f2(actor, ul);
        };
    // console.log(ul);
    all_actors[i] = ul;
    i++;
    }
    // console.log(all_actors);
    return all_actors;
}

function writeActors(arr) {
    let all_li = document.querySelectorAll("li");
    let all_div1 = document.querySelectorAll(".div0");
    let num = 0;

    all_div1.forEach(
        (elem) => {
            elem.remove();
        }
    );

    arr.forEach(
        (elem) => {
            all_li[num].append(elem);
            num++;
        }
    );
}

async function f1(data) {
    // console.log(data);
    let characters = [];
    let ul = document.createElement("ul");
    document.body.append(ul);
    data.forEach(
        (obj) => {
            let li = document.createElement("li");
            li.innerHTML = `
                <p>Episode: ${obj.episodeId}</p>
                <p>Name of film: ${obj.name}</p>
                <p>Abstract: ${obj.openingCrawl}</p>
                <div class="div0">
                    <div class="div1">S</div>
                    <div class="div2">W</div>
                    <div class="div3">T</div>
                    <div class="div4">A</div>
                    <div class="div5">A</div>
                    <div class="div6">R</div>
                    <div class="div7">R</div>
                    <div class="div8">S</div>
                </div>
            `;
            ul.append(li);

            characters.push(obj.characters);
        }
    );
    return characters;
}

async function f2 (data, dom_element) {
    // console.log(data);
    let li = document.createElement("li");
    dom_element.append(li);
    li.textContent = `
        Name: ${data.name}, ${data.gender}, eyes: ${data.eyeColor}, height: ${data.height}cm.
    `;
}


getStraWarsFilms();

