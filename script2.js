function getStraWarsFilms() {
    document.body.innerHTML = "";
    fetch("https://ajax.test-danit.com/api/swapi/films")
        .then(
            data => data.json()
        )
        .then(
            (data) => {
                console.log(data);
                let characters = [];
                let ul = document.createElement("ul");
                document.body.append(ul);
                data.forEach(
                    (obj) => {
                        let li = document.createElement("li");
                        li.innerHTML = `
                            <p>Episode: ${obj.episodeId}</p>
                            <p>Name of film: ${obj.name}</p>
                            <p>Abstract: ${obj.openingCrawl}</p>
                            <div class="div0">
                                <div class="div1">S</div>
                                <div class="div2">W</div>
                                <div class="div3">T</div>
                                <div class="div4">A</div>
                                <div class="div5">A</div>
                                <div class="div6">R</div>
                                <div class="div7">R</div>
                                <div class="div8">S</div>
                            </div>
                        `;
                        ul.append(li);

                        characters.push(obj.characters);
                    }
                );
                return characters;
            }
        )
        .then(
           (data) => {
                console.log(data);
                return getActors(data);
            }
    )
        .then(
            (data) => {
                console.log(data);
                writeActors(data);
            }
        )
}


async function getActors(characters) {

    let all_actors = [];
    let i = 0;

    for (let elem of characters) { 
            let ul = document.createElement("ul");
            for (let arr_elem of elem) {
                await fetch(arr_elem)
                    .then(
                        data => data.json()
                    )
                    .then(
                        (data) => {
                            console.log(data);
                            let li = document.createElement("li");
                            ul.append(li);
                            li.textContent = `
                                    Name: ${data.name}, ${data.gender}, eyes: ${data.eyeColor}, height: ${data.height}cm.
                                    `;
                        }
                    );
            };
            console.log(ul);
        all_actors[i] = ul;
        i++;
    }
    console.log(all_actors);
    return all_actors;
}

function writeActors(arr) {
    let all_li = document.querySelectorAll("li");
    let all_div1 = document.querySelectorAll(".div0");
    let num = 0;

    all_div1.forEach(
        (elem) => {
            elem.remove();
        }
    );

    arr.forEach(
        (elem) => {
            all_li[num].append(elem);
            num++;
        }
    );
}


getStraWarsFilms();

